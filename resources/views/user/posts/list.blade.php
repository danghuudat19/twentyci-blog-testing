@extends('user.app')
@section('content')
    <div class="container">
        <a class="btn btn-primary" href="/user/post-add" role="button">Create post</a>
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif
        @if (\Session::has('error'))
            <div class="alert alert-danger">
                <p>{{ \Session::get('error') }}</p>
            </div>
        @endif
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Title</th>
                <th scope="col">Status</th>
                <th scope="col">Publish at</th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($listPost as $post)
                <tr>
                    <th scope="row">{{$post->id}}</th>
                    <td>{{$post->title}}</td>
                    <td>
                        @if($post->status == 1)
                            Published
                        @else
                            Unpublished
                        @endif
                    </td>
                    <td>{{$post->publish_at}}</td>
                    <td><a href="/user/post-edit/{{$post->id}}">Edit</a></td>
                    <td>
                        <form action="/user/delete-post/{{$post->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button>Delete</button>
                        </form>
                        <form action="/user/3" method="POST">
                            @csrf
                            @method('DELETE')
                            <button>Delete User</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="/user/list-post?page=" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">First page</span>
                    </a>
                </li>
                @for($i = 0; $i < $listPost->lastPage(); $i ++)
                    <li class="page-item"><a class="page-link" href="/admin?page={{$i+1}}">{{$i+1}}</a></li>
                @endfor
                <li class="page-item">
                    <a class="page-link" href="/user/list-post?page={{$listPost->lastPage()}}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Last page</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
@endsection
