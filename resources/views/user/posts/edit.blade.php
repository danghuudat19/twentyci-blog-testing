@extends('user.app')
@section('content')

    <div class="container">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif
        @if (\Session::has('error'))
            <div class="alert alert-danger">
                <p>{{ \Session::get('error') }}</p>
            </div>
        @endif
            <form action="/user/edit/{{$post->id}}" class="form" method="post">
                @csrf
                <div class="form-group mb-2">
                    <label for="title" class="">Title</label><br>
                    <input type="text" name="title" id="title" class="form-control" required value="{{$post->title}}">
                    @error('title')
                    <div class="validate-error-text">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body" class="">Body</label><br>
                    <textarea name="body" required>
                        {{$post->body}}
                    </textarea>
                    @error('body')
                    <div class="validate-error-text">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit">Submit</button>
            </form>

            <script>
                tinymce.init({
                    selector: 'textarea',
                    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
                    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
                    toolbar_mode: 'floating',
                    tinycomments_mode: 'embedded',
                    tinycomments_author: 'Author name',
                });
            </script>

    </div>
@endsection
