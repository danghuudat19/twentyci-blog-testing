<!DOCTYPE html>
<html lang="eng">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>

<head>
    @include('user.master')
</head>

<body>
    @include('user.layouts.header')
    @yield('content')
    @include('user.layouts.footer')
</body>
@stack('scripts')
</html>
