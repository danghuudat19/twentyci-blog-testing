@extends('user.app')
@section('content')
    <div id="login" class="pb-5">
        <h3 class="text-center">Register form</h3>
        <div class="container">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <p>{{ \Session::get('error') }}</p>
                </div>
            @endif
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="/register/store" method="post">
                            @csrf
                            <h3 class="text-center">Register</h3>
                            <div class="form-group">
                                <label for="name" class="">Name:</label><br>
                                <input type="text" name="name" id="name" class="form-control" required>
                                @error('name')
                                <div class="validate-error-text">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email" class="">Email:</label><br>
                                <input type="email" name="email" id="email" class="form-control" required>
                                @error('email')
                                <div class="validate-error-text">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group pb-2">
                                <label for="password" class="">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control" required>
                                @error('password')
                                <div class="validate-error-text">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group pb-2">
                                <label for="password_confirmation" class="">Password Confirmation:</label><br>
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required>
                                @error('password_confirmation')
                                <div class="validate-error-text">{{ $message }}</div>
                                @enderror
                            </div>
                            <input type="submit" name="submit" class="btn btn-info btn-md pb-2" value="submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
