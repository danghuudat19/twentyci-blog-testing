@extends('user.app')
@section('content')
<div class="container px-4 px-lg-5">
    <div class="row gx-4 gx-lg-5 justify-content-center">
        <div class="col-md-10 col-lg-8 col-xl-7">
            <!-- Post preview-->
            @foreach($listPost as $post)
            <div class="post-preview">
                <a href="/post/{{$post->id}}">
                    <h2 class="post-title">{{$post->title}}</h2>
                </a>
                <p class="post-meta">
                    Posted by

                    <br>
                    {{$post->publish_at}}
                </p>
            </div>
            <!-- Divider-->
            <hr class="my-4" />
            @endforeach
            <!-- Pager-->
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="/user/list-post?page=1" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    @for($i = 0; $i < $listPost->lastPage(); $i ++)
                        <li class="page-item"><a class="page-link" href="/user/list-post?page={{$i+1}}">{{$i+1}}</a></li>
                    @endfor
                    <li class="page-item">
                        <a class="page-link" href="/user/list-post?page={{$listPost->lastPage()}}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
@endsection
