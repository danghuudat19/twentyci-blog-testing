@extends('user.app')
@section('content')
    <div class="container">
        <form method="post" action="/admin/filter">
            @csrf
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Date filter</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="publish_at" id="gridRadios1" value="desc"
                            @if($publishAt == 'desc')
                                checked
                            @endif
                            >
                            <label class="form-check-label" for="gridRadios1">
                                From newest
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="publish_at" id="gridRadios2" value="asc"
                            @if($publishAt == 'asc')
                               checked
                            @endif
                            >
                            <label class="form-check-label" for="gridRadios2">
                                From oldest
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="publish_at" id="gridRadios2" value="none"
                            @if($publishAt == 'none')
                               checked
                            @endif
                            >
                            <label class="form-check-label" for="gridRadios2">
                                No search
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Status filter</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="gridRadios1" value="1"
                            @if($status == '1')
                               checked
                            @endif
                            >
                            <label class="form-check-label" for="gridRadios1">
                                Publish
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="gridRadios2" value="0"
                            @if($status == '0')
                                checked
                            @endif
                            >
                            <label class="form-check-label" for="gridRadios2">
                                UnPublished
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="gridRadios2" value="none"
                            @if($status == 'none')
                               checked
                            @endif
                            >
                            <label class="form-check-label" for="gridRadios2">
                                No search
                            </label>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="/admin">Reset form</a>
                </div>
            </div>
        </form>
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif
        @if (\Session::has('error'))
            <div class="alert alert-danger">
                <p>{{ \Session::get('error') }}</p>
            </div>
        @endif
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Title</th>
                <th scope="col">Author</th>
                <th scope="col">Status</th>
                <th scope="col">Publish at</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($listPost as $post)
                <tr>
                    <th scope="row">{{$post->id}}</th>
                    <td>{{$post->title}}</td>
                    <td></td>
                    <td>
                        @if($post->status == 1)
                            Published
                        @else
                            Unpublished
                        @endif
                    </td>
                    <td>{{$post->publish_at}}</td>
                    <td>@if(!$post->status)
                        <form action="/admin/schedule-publish-post" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{$post->id}}">
                            <input type="datetime-local" id="schedule_publish" name="schedule_publish" required>
                            <button type="submit">
                                Schedule publish
                            </button>
                        </form>
                        @endif
                    </td>
                    <td>
                        <form action="/admin/publish-post" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{$post->id}}">
                            <button>
                                @if(!$post->status)
                                    Publish Now
                                @else
                                    Unpublished Now
                                @endif
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="/admin?page=1" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">First page</span>
                    </a>
                </li>
                @for($i = 0; $i < $listPost->lastPage(); $i ++)
                    <li class="page-item"><a class="page-link" href="/admin?page={{$i+1}}">{{$i+1}}</a></li>
                @endfor
                <li class="page-item">
                    <a class="page-link" href="/admin?page={{$listPost->lastPage()}}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Last page</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
@endsection
