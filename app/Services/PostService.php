<?php

namespace App\Services;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PostService
{
    protected Post $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function getActiveList(){
        return Post::query()->where('status','=',Post::STATUS_PUSHLISH)->with('user')->paginate();
    }

    public function getDetail($id){
        return Post::query()->with('user')->find($id);
    }

    public function getListByCurrentUser(){
        $user_id = Auth::id();
        return Post::query()->where('user_id','=', $user_id)->paginate();
    }

    public function delete($id){
        $post = Post::query()->find($id);
        $post->delete();
        $post->save();
        return $post;
    }

    public function getAllPost(){
        return Post::query()->with('user')->paginate();
    }

    public function publicPost($id){
        $post = Post::query()->find($id);
        if(!$post->status){
            $post->status = true;
            $post->publish_at = Carbon::now();
            $msg = 'Published success';
        }else{
            $post->status = false;
            $post->publish_at = null;
            $msg = 'Unpublished success';
        }
        $post->save();
        return $msg;
    }

    public function filterPost($request){
        $query = Post::query();
        if($request->get('status') != "none"){
            $query->where('status', '=', $request->get('status'));
        } elseif ($request->get('publish_at') != "none"){
            $query->orderBy('publish_at', $request->get('publish_at'));
        }
        return $query->paginate();
    }

    public function schedulePublicPost($request){
        $post = Post::query()->find($request->get('id'));
        $post->publish_at = $request->get('schedule_publish');
        $post->save();
        $msg = "Schedule success";
        return $msg;
    }

    public function autoPublishPost(){
        $post = Post::query()
            ->where('status', '=', Post::STATUS_UNPUSHLISH)
            ->where('publish_at', '<=', date("Y-m-d H:i:s"))
            ->update(['status' => Post::STATUS_PUSHLISH]);
        return $post;
    }

    public function editPost($id, $request){
        $post = Post::query()->find($id);
        $post->body = $request->get('body');
        $post->title = $request->get('title');
        $post->save();
        $msg = "Edit successfully";
        return $msg;
    }

    public function addPost($request){
        $post['body'] = $request->get('body');
        $post['title'] = $request->get('title');
        $post['user_id'] = Auth::id();
        Post::query()->insert($post);
        $msg = "Create post success";
        return $msg;
    }
}

