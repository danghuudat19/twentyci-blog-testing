<?php


namespace App\Services;



use App\Models\User;
use Illuminate\Support\Facades\{Auth, Hash};

class UserService
{
    protected User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function get($id)
    {
        return User::query()->find($id);
    }

    public function store($request)
    {
        $user = new User();

        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->name = $request->name;

        $user->save();
        return $user;
    }
    public function checkAdmin()
    {
        $user = Auth::user();
        return $user->is_admin;
    }
}
