<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Services\PostService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PostController extends Controller
{
    protected PostService $postService;
    protected UserService $userService;

    public function __construct(PostService $postService, UserService $userService)
    {
        $this->postService = $postService;
        $this->userService = $userService;
    }

    public function getDetail($id){
        $post = $this->postService->getDetail($id);
        return view('user.posts.detail')->with(['post' => $post]);
    }

    public function getListByCurrentUser(){
        $listPost = $this->postService->getListByCurrentUser();
        return view('user.posts.list')->with(['listPost' => $listPost]);
    }

    public function delete($id){
        try {
            $this->postService->delete($id);
            $listPost = $this->postService->getListByCurrentUser();
            return redirect('/user/list-post')->with(['listPost' => $listPost, 'success' => 'Delete success']);
        }
        catch (\Exception $exception){
            $listPost = $this->postService->getListByCurrentUser();
            return Redirect::back()->with(['listPost' => $listPost, 'error' => 'Delete failed']);
        }
    }

    public function getEdit($id){
        $post = $this->postService->getDetail($id);
        return view('user.posts.edit')->with(['post' => $post]);
    }

    public function edit($id, PostRequest $request){
        try {
            $msg = $this->postService->editPost($id, $request);;
            return redirect('/user/list-post')->with(['success' => $msg]);
        }
        catch (\Exception $exception){
            $post = $this->postService->getDetail($id);
            return Redirect::back()->with(['post' => $post, 'error' => 'Edit failed']);
        }
    }

    public function getAdd(){
        return view('user.posts.add');
    }

    public function store(PostRequest $request){
        try {
            $msg = $this->postService->addPost($request);
            return redirect('/user/list-post')->with(['success' => $msg]);
        }
        catch (\Exception $exception){
            \Log::info($exception);
            return Redirect::back()->with([ 'error' => 'Create failed']);
        }
    }
}
