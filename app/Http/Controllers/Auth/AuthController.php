<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AuthController
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        if (Auth::check()) {
            return redirect('/');
        }
        return view('login');
    }

    public function login(LoginRequest $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($data)) {
            /** @var User $user */
            $request->session()->regenerate();
            return Auth::user()->is_admin ? redirect()->intended('/admin') : redirect()->intended('/');
        }
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function detail()
    {
        return auth()->user();
    }

    public function getRegister()
    {
        if (Auth::check()) {
            return redirect('/');
        }
        return view('register');
    }

    public function store(RegisterRequest $request)
    {
        try {
            $this->userService->store($request);
            return Redirect::back()->with(['success' => 'Register success']);
        }
        catch (\Exception $exception){
            \Log::info($exception);
            return Redirect::back()->with(['error' => 'Register failed']);
        }
    }

    public function delete($id){
        User::destroy($id);
        return Redirect::route('home');
    }
}
