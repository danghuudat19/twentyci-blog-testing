<?php

namespace App\Http\Controllers;

use App\Services\PostService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    protected PostService $postService;
    protected UserService $userService;

    public function __construct(PostService $postService, UserService $userService)
    {
        $this->postService = $postService;
        $this->userService = $userService;
    }

    public function getAllPost(){
        $listPost = $this->postService->getAllPost();
        $publishAt = 'none';
        $status = 'none';
        return view('admin.posts.list')->with(['listPost' => $listPost, 'publishAt' => $publishAt, 'status' => $status]);
    }

    public function publicPost(Request $request){
        try {
            $id = $request->get('id');
            $msg = $this->postService->publicPost($id);
            $listPost = $this->postService->getListByCurrentUser();
            return Redirect::back()->with(['listPost' => $listPost, 'success' => $msg]);
        }
        catch (\Exception $exception){
            $listPost = $this->postService->getListByCurrentUser();
            return Redirect::back()->with(['listPost' => $listPost, 'error' => 'Public post failed']);
        }
    }

    public function filterPost(Request $request){
        $listPost = $this->postService->filterPost($request);
        $publishAt = $request->get('publish_at');
        $status = $request->get('status');
        return view('admin.posts.list')->with(['listPost' => $listPost, 'publishAt' => $publishAt, 'status' => $status]);
    }

    public function schedulePublicPost(Request $request){
        try {
            $msg = $this->postService->schedulePublicPost($request);
            $listPost = $this->postService->getListByCurrentUser();
            return Redirect::back()->with(['listPost' => $listPost, 'success' => $msg]);
        }
        catch (\Exception $exception){
            $listPost = $this->postService->getListByCurrentUser();
            return Redirect::back()->with(['error' => 'Schedule failed', 'listPost' => $listPost]);
        }

    }
}
