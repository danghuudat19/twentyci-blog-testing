<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Factories\HasFactory, Model, Relations\BelongsTo, SoftDeletes};

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

    public const STATUS_PUSHLISH = 1;
    public const STATUS_UNPUSHLISH = 0;

    protected $fillable = [
        'user_id',
        'title',
        'body',
        'status',
        'publish_at',
    ];


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
