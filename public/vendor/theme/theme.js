(function($) {
    $('.home-slider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
    });

    //step by step
    var pages = ["#one","#two","#three","#four","#five"];
    $(".next").click(function(){
        var temp = 0;
        var nextElementFound = false;
        pages.forEach(function(element) {
            temp++;
            if ($(element).is(":visible")){
                current_fs =$(element);
                if(pages.length > temp){
                    next_fs =$(pages[temp]);
                    nextElementFound = true;
                }
            }
        });
        if(nextElementFound){
            next_fs.show();
            current_fs.hide();
        }
    });

    $('.previous').click(function(){
        var temp = 0;
        var previousElementFound = false;
        pages.forEach(function(element) {
            if ($(element).is(":visible")){
                current_fs =$(element);
                if(temp != 0){
                    previousElementFound= true;
                    next_fs =$(pages[temp-1]);
                }
            }
            temp++;
        });
        if(previousElementFound){
            next_fs.show();
            current_fs.hide();
        }
    });

    // header
    $(window).scroll(function() {
        const scroll = $(window).scrollTop();
        const offsetTop = $('.header-container').height();

        if (scroll >= offsetTop) {
            $(".main-menu").addClass("active");
        } else {
            $(".main-menu").removeClass("active");
        }
    });


    $('.animation').each(function( ) {
        var top_of_element = $(this).offset().top;
        var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();

        if ((bottom_of_screen - top_of_element > 50)){
            $(this).addClass('active-animation');
        } else {
            $(this).removeClass('active-animation');
        }
    });

    $(window).scroll(function(){
        $('.animation').each(function( ) {
            var top_of_element = $(this).offset().top;
            var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();

            if ((bottom_of_screen - top_of_element > 50)){
                $(this).addClass('active-animation');
            } else {
                $(this).removeClass('active-animation');
            }
        });
    })

    $('.toggle-menu').click(function () {
        $('.main-menu').toggleClass('mobile-menu-active');
    });

    $('.close-menu').click(function () {
        $('.main-menu').removeClass('mobile-menu-active');
    });

    $('.has-child-icon').click(function () {
        $(this).parent('.has-child').toggleClass('expand-child');
    });
})(jQuery);