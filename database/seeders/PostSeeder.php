<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::query()->where('is_admin', '=', false)->first();
        Post::factory()
            ->count(10)
            ->create([
                'user_id' => $user->id, 'status' => rand(0, 1),
                'publish_at' => date("Y-m-d H:i:s",mt_rand(1262055681,1262055681))
            ]);
    }
}
