<?php
use App\Http\Controllers\PostController;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\{HomeController,
    AdminController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout']);
Route::post('/logout', [AuthController::class, 'logout']);

Route::prefix('/register')->group(function () {
    Route::get('/', [AuthController::class, 'getRegister']);
    Route::post('/store', [AuthController::class, 'store']);
});

Route::prefix('/post')->group(function () {
    Route::get('/{id}', [PostController::class, 'getDetail']);
});

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('/user')->group(function () {
        Route::get('/list-post', [PostController::class, 'getListByCurrentUser']);
        Route::delete('/delete-post/{id}', [PostController::class, 'delete']);
        Route::get('/post-edit/{id}', [PostController::class, 'getEdit']);
        Route::post('/edit/{id}', [PostController::class, 'edit']);
        Route::get('/post-add', [PostController::class, 'getAdd']);
        Route::post('/post-add', [PostController::class, 'store']);
        Route::get('/', [AuthController::class, 'detail']);
        Route::delete('/{id}', [AuthController::class, 'delete']);

    });
    Route::group(['middleware' => 'checkAdmin', 'prefix'=>'/admin'], function () {
        Route::get('/', [AdminController::class, 'getAllPost']);
        Route::post('/publish-post', [AdminController::class, 'publicPost']);
        Route::post('/schedule-publish-post', [AdminController::class, 'schedulePublicPost']);
        Route::post('/filter', [AdminController::class, 'filterPost']);
    });
});


